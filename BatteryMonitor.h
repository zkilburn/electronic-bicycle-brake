#include <stdint.h>
#include <stdbool.h>

//BATTERY MONITOR TUNING
#define VOLTAGE_DIVIDER_RATIO     0.319
#define MIN_BATTERY_VOLTAGE       8.4
#define MAX_BATTERY_VOLTAGE       12.6
#define MIN_VOLTAGE_ADC_VALUE     MIN_BATTERY_VOLTAGE * VOLTAGE_DIVIDER_RATIO * 1023 / 5.0
#define MAX_VOLTAGE_ADC_VALUE     MAX_BATTERY_VOLTAGE * VOLTAGE_DIVIDER_RATIO * 1023 / 5.0

class BatteryMonitor
{
  public:
    BatteryMonitor(void);
    void runBatteryMonitor(bool * forceDisplay);
    void debugBatteryRead(void);
  private:
    uint16_t readBatteryLevel(void);
};

