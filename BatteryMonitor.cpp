#include "BatteryMonitor.h"
#include "PinMapping.h"
#include "Timers.h"


//Variables
Timers battMonitorTimer(5000);

BatteryMonitor::BatteryMonitor(void)
{
  //Pins for batt monitor
  pinMode(LED_R_PIN,OUTPUT);
  pinMode(LED_Y_PIN,OUTPUT);
  pinMode(LED_G1_PIN,OUTPUT);
  pinMode(LED_G2_PIN,OUTPUT);
  pinMode(LED_G3_PIN,OUTPUT);
  pinMode(BATTERY_VOLTAGE_PIN,INPUT);  
}

/*  Force display will make the system operate, even if not charging */
void BatteryMonitor::runBatteryMonitor(bool * forceDisplay)
{  
  if(digitalRead(CHARGING_INDICATOR_PIN) || *forceDisplay)          //If the battery monitor button is pressed
  {
    uint8_t battLevel = readBatteryLevel();   //Read the battery level

    if(battLevel > 75)                        //LED output of batt level
    {
      digitalWrite(LED_G1_PIN, LOW);
      digitalWrite(LED_G2_PIN, LOW);
      digitalWrite(LED_G3_PIN, LOW);
      digitalWrite(LED_Y_PIN,  LOW);
      digitalWrite(LED_R_PIN,  LOW);
    }
    else if(battLevel > 60)
    {
      digitalWrite(LED_G1_PIN, HIGH);
      digitalWrite(LED_G2_PIN, LOW);
      digitalWrite(LED_G3_PIN, LOW);
      digitalWrite(LED_Y_PIN,  LOW);
      digitalWrite(LED_R_PIN,  LOW);
    }
    else if(battLevel > 45)
    {
      digitalWrite(LED_G1_PIN, HIGH);
      digitalWrite(LED_G2_PIN, HIGH);
      digitalWrite(LED_G3_PIN, LOW);
      digitalWrite(LED_Y_PIN,  LOW);
      digitalWrite(LED_R_PIN,  LOW);
    }
    else if(battLevel > 30)
    {
      digitalWrite(LED_G1_PIN, HIGH);
      digitalWrite(LED_G2_PIN, HIGH);
      digitalWrite(LED_G3_PIN, HIGH);
      digitalWrite(LED_Y_PIN,  LOW);
      digitalWrite(LED_R_PIN,  LOW);
    }
    else
    {
      digitalWrite(LED_G1_PIN, HIGH);
      digitalWrite(LED_G2_PIN, HIGH);
      digitalWrite(LED_G3_PIN, HIGH);
      digitalWrite(LED_Y_PIN,  HIGH);
      digitalWrite(LED_R_PIN,  LOW);
    }
    battMonitorTimer.resetTimer();
  }

  if(battMonitorTimer.timerDone() && *forceDisplay)    //DISPLAY READOUT FOR A TIME, THEN SHUT OFF
  {
      digitalWrite(LED_G1_PIN, LOW);
      digitalWrite(LED_G2_PIN, LOW);
      digitalWrite(LED_G3_PIN, LOW);
      digitalWrite(LED_Y_PIN,  LOW);
      digitalWrite(LED_R_PIN,  LOW);
      *forceDisplay = false;
  }
}

uint16_t BatteryMonitor::readBatteryLevel(void)
{
  int batterySOCApprox=0;
  batterySOCApprox=map(analogRead(BATTERY_VOLTAGE_PIN), MIN_VOLTAGE_ADC_VALUE, MAX_VOLTAGE_ADC_VALUE, 0, 100);   //Map battery voltage to the SOC values
  return batterySOCApprox;
}

void BatteryMonitor::debugBatteryRead(void)
{
  static Timers battDebugTimer(1000);
  if(battDebugTimer.timerDone())
  {
    Serial.print("Battery Read: ");
    Serial.print(analogRead(BATTERY_VOLTAGE_PIN));  //Display the raw ADC Reading
    Serial.print(", Battery SOC: ");
    Serial.println(map(analogRead(BATTERY_VOLTAGE_PIN), MIN_VOLTAGE_ADC_VALUE, MAX_VOLTAGE_ADC_VALUE, 0, 100));   //Map battery voltage to the SOC values
  }
  
}


