// Servo Functions and Operation
#include <stdbool.h>
#include <stdint.h>
#include "Arduino.h"

class BrakeServo
{
  public:
    BrakeServo(void);
    void updateServo(bool buttonDepressed);
    void testServoMovement(void);
    void setPwmFrequency(int pin, int divisor);
  private:
    void rampBrakeUp(void);
    void rampBrakeDown(void);
    void zeroServo(void);
  
  
};
