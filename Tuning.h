//READABILITY DEFINES
#define BRAKE_PRESSED             false

#define uS_MIN_RANGE                      1550.0
#define uS_MAX_RANGE                      2250.0
#define uS_PERIOD_PWM                     4160.0
#define MAX_DUTY_ANALOG_OUT               255.0

#define CLOCKWISE                         1
#define BRAKE_MIN_ANGLE_CLOCKWISE         uS_MAX_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   //uS * (MAX_DUTY_COUNTS/uS Period)  
#define BRAKE_ENGAGEMENT_ANGLE_CLOCKWISE  uS_MAX_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   //uS * (MAX_DUTY_COUNTS/uS Period)  
#define BRAKE_MAX_ANGLE_CLOCKWISE         uS_MIN_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   //uS * (MAX_DUTY_COUNTS/uS Period)  

//Increment value 
#define INC                       1
//BRAKE TUNING (SERVO)
#define BRAKE_MIN_ANGLE           uS_MIN_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   //uS * (MAX_DUTY_COUNTS/uS Period)  
#define BRAKE_ENGAGEMENT_ANGLE    uS_MIN_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   
#define BRAKE_MAX_ANGLE           uS_MAX_RANGE * (MAX_DUTY_ANALOG_OUT/uS_PERIOD_PWM)   
//112   

//BRAKE TUNING (RAMPING)
#define RAMP_TUNED_TIMING         25
#define RAMP_DOWN_TIMING          12


