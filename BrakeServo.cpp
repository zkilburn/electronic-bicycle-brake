#include "Tuning.h"
#include "PinMapping.h"
#include "BrakeServo.h"
#include <Timers.h>

//Servo Variables
bool brakeApplied=false;
uint8_t brakeEngagementAngle=0;
bool goingDown=false;
Timers rampUpTiming(RAMP_TUNED_TIMING), rampDownTiming(RAMP_DOWN_TIMING);

BrakeServo::BrakeServo(void)
{  
  //Set Servo Pin Time Period
  this->setPwmFrequency(3, 128);                      //32  - 960 Hz (1.04 ms)
                                                      //64  - 480 Hz (2.08 ms)
                                                      //128 - 240   (4.16 ms)
                                                      //256 - 120   (8.32 ms)
                                                      //512 Divider gives -60Hz - (16.64 ms)  
                                                      //1024 div gives 30Hz
                            
  pinMode(SERVO_PIN,OUTPUT);                          //Setupt the servo pin
  analogWrite(SERVO_PIN, BRAKE_MIN_ANGLE);            //Set the servo initialization
  
  #ifdef CLOCKWISE
    brakeEngagementAngle=BRAKE_MIN_ANGLE_CLOCKWISE;   //Set the init position
  #else
    brakeEngagementAngle=BRAKE_MIN_ANGLE;             //Set the init position    
  #endif  
}

void BrakeServo::updateServo(bool buttonDepressed)
{
    if(buttonDepressed==BRAKE_PRESSED)
    {
      this->rampBrakeUp();
    }
    else
    {
      this->rampBrakeDown();
    }    
}

/************* SUPPLEMENTAL FUNCTIONS *************/
void BrakeServo::rampBrakeUp(void)
{
  #ifndef CLOCKWISE
    if(brakeEngagementAngle == 0)               //Brake is fully disengaged
    {
      analogWrite(SERVO_PIN,BRAKE_ENGAGEMENT_ANGLE);    //Move the servo to engagement
      brakeEngagementAngle = BRAKE_ENGAGEMENT_ANGLE;    //Record position
    }
    else                                        //Brake is at engagement level or greater
    {
      if(rampUpTiming.timerDone())
      {
        if(brakeEngagementAngle < BRAKE_MAX_ANGLE)
        {
          brakeEngagementAngle+=INC;
          analogWrite(SERVO_PIN,brakeEngagementAngle);
        }
      }
    }
  #else
    if(brakeEngagementAngle == BRAKE_MIN_ANGLE_CLOCKWISE)               //Brake is fully disengaged
    {
      analogWrite(SERVO_PIN,BRAKE_ENGAGEMENT_ANGLE_CLOCKWISE);    //Move the servo to engagement
      brakeEngagementAngle = BRAKE_ENGAGEMENT_ANGLE_CLOCKWISE;    //Record position
    }
    else                                        //Brake is at engagement level or greater
    {
      if(rampUpTiming.timerDone())
      {
        if(brakeEngagementAngle > BRAKE_MAX_ANGLE_CLOCKWISE)
        {
          brakeEngagementAngle-=INC;
          analogWrite(SERVO_PIN,brakeEngagementAngle);          
        }
      }
    }
  #endif
} /* RAMP BRAKE UP */

void BrakeServo::rampBrakeDown(void)
{
  #ifdef CLOCKWISE
    if(brakeEngagementAngle<BRAKE_MIN_ANGLE_CLOCKWISE)
    {
      if(rampDownTiming.timerDone())
      {
        analogWrite(SERVO_PIN,brakeEngagementAngle++);
        brakeEngagementAngle+= INC;
      }
    }
  #else 
    if(brakeEngagementAngle>BRAKE_MIN_ANGLE)
    {
      if(rampDownTiming.timerDone())
      {
        analogWrite(SERVO_PIN,brakeEngagementAngle--);
        brakeEngagementAngle-= INC;
      }
    }
  #endif
} /* RAMP BRAKE DOWN */

void BrakeServo::zeroServo(void)
{
  #ifndef CLOCKWISE
    analogWrite(SERVO_PIN,BRAKE_MIN_ANGLE);
    brakeEngagementAngle=BRAKE_MIN_ANGLE;
  #else
    analogWrite(SERVO_PIN,BRAKE_MIN_ANGLE_CLOCKWISE);
    brakeEngagementAngle=BRAKE_MIN_ANGLE_CLOCKWISE;
  #endif  
} /* ZERO SERVO */

void BrakeServo::testServoMovement(void)
{
  #ifdef CLOCKWISE
    if(brakeEngagementAngle > BRAKE_MAX_ANGLE_CLOCKWISE && !goingDown)
    {
      this->rampBrakeUp();
    }
    else if(brakeEngagementAngle < BRAKE_MIN_ANGLE_CLOCKWISE)
    {
      goingDown=true;
      this->rampBrakeDown();
    }  
    else
    {
      goingDown=false;
    }
  #else
    if(brakeEngagementAngle < BRAKE_MAX_ANGLE && !goingDown)
    {
      this->rampBrakeUp();
    }
    else if(brakeEngagementAngle > BRAKE_MIN_ANGLE+1)
    {
      goingDown=true;
      this->rampBrakeDown();
    }  
    else
    {
      goingDown=false;
    }
  #endif
  
  Serial.print("Servo Angle: ");
  Serial.println(brakeEngagementAngle);  
} /* TEST SERVO MOVEMENT */

void BrakeServo::setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}



