
//DIGITAL PINS
#define SERVO_PIN                 3
#define BUTTON_INPUT_PIN          2
#define LED_R_PIN                 5
#define LED_Y_PIN                 6
#define LED_G3_PIN                7
#define LED_G2_PIN                8
#define LED_G1_PIN                9

//ANALOG PINS
#define BATTERY_VOLTAGE_PIN       A7
#define CHARGING_INDICATOR_PIN    A6
