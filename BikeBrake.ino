#include "Timers.h"
#include "Tuning.h"
#include "PinMapping.h"
#include "BrakeServo.h"
#include "BatteryMonitor.h"

bool bootupPowerDisplay = true;
BatteryMonitor battery;
BrakeServo brake;

void setup() 
{
  //Debugging serial
  Serial.begin(115200);
  pinMode(BUTTON_INPUT_PIN,INPUT_PULLUP);
}

void loop() 
{  
  //brake.testServoMovement();                            //Debug/test the servo movement
  brake.updateServo(digitalRead(BUTTON_INPUT_PIN));   //Read the button and use the brake as necessary

  //battery.debugBatteryRead();                         //Print the battery info to the terminal
  battery.runBatteryMonitor(&bootupPowerDisplay);        //Use the battery read to display battery life
}



